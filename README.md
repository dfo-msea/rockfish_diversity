# Analysis of Inshore Rockfish biodiversity overlap with RCAs
Led by Patrick Thompson and Dana Haggarty

patrick.thompson@dfo-mpo.gc.ca

Started in May 2022

This project is using model outputs from an integrated model of groundfish distributions developed by Thompson et al. (in review) to estimate inshore biodiversity and overlap with Rockfish Conservation Areas.